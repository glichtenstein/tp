# -*- coding: utf-8 -*-
#!/usr/bin/env python
########################################################################
# Trabajo Práctico - Grupo 4
# Ejercicio 1 - En tiempos de euclídea
# Autores: Agustin Dramis y Gabriel Lichtenstein
# Fecha: 03/06/2016
########################################################################
# usage: python ejercicio1.py
########################################################################
import numpy as np

# Importar funciones auxiliares:
from cronometro import cronometro
from puntos_random import entradaRandom
from distancia_euclidea import distanciaMinima, distanciaMinimaDyC
from preprocesamiento import merge, up, bubble, quick

# Importar MatPlotLib
try: # ojo matplotlib funciona mejor en python2
	import matplotlib.pyplot as plt
except ImportError:
	print("\n►Disculpas, matplotlib funciona mejor en Python 2\n" 
	+ "	Prueba p.ej: python2.7 ejercicio1.py\n")
	exit()
########################################################################
#
#
########################################################################
# Lista de Puntos - carga un archivo
########################################################################
def listaDePuntos(fn):
	'''
	Función que toma un archivo como parametro y lo lee, y por cada 
	una de sus lineas crea una lista de números (convertidos a float)
	separados por espacio. Luego convierte cada una de esas listas 
	en una tupla, y finalmente devuelve una lista con todas las 
	tuplas (que son pares de coordenadas en 2D).
	'''
	res = []
	with open(fn, "r") as f:
		for line in f:
			line = line.rstrip('\n')
			line = line.split(' ')
			for n in range(len(line)):
				line[n] = float(line[n])
			res.append(tuple(line))
	return res
########################################################################
#
#
########################################################################
# Formatear resultados para armar un csv con los datos a graficar:
########################################################################
def fila_de_datos(n):
	'''
	1) Mediante la fn aux entradaRandom() se crea un archivo de 
	"entrada.txt" que contiene lineas con pares de puntos "random" 
	separados por "<espacio>". 
	La cantidad de pares de puntos (a.k.a líneas) esta determinado 
	por el valor de "n".
	
	2) Se cargan los valores de entrada.txt y se los pasa a una lista.
	Cada par de puntos "random" es convertido a una tupla (x,y) y lusgo 
	son agregados a la lista "lista" mediante la fn aux listaDePuntos().
	
	3) Función principal. Se calcula la distanciaMinima euclídea de los
	pares de puntos contenidos en el archivo "entrada.txt".
	Se utiliza un función aux cronometro para almacenar en una lista 
	"[fila_de_datos]" el "tiempo de ejecución" del algoritmo utilizado 
	para calcular dicha distancia.
	
	La lista consta de 6 elementos (a.k.a columnas):
	  i) n= "tamaño de entrada.txt (a.k.a cantidad de pares de puntos)"
	 ii) distanciaMinima = tiempo de ejecución del algoritmo de FuerzaBruta
	iii) merge = tiempo de ejecución del algoritmo DyC y sorting por merge
     iv) up = tiempo de ejecución del algoritmo DyC y sorting por up
	  v) quick = tiempo de ejecución del algoritmo DyC y sorting por quick
	 vi) bubble = tiempo de ejecución del algoritmo DyC y sorting por bubble
	
	Como resultado devuelve una lista que representa una fila_de_datos
	que luego va a ser agregada al archivo datos.csv con el fín de 
	realizar un plot.
	'''		
	entradaRandom(n, "entrada.txt") # crea pares de puntos de entrada
	# se hardcodea el nombre entrada.txt para que se sobreescriba
	# el archivo cuando se quieren hacer muchos conjuntos de de datos
	# de tamaño variable, p.ej: de 2 a 1000 pares de puntos.
	
	lista = listaDePuntos("entrada.txt") # deja la entrada lista :)
	
	fila_de_datos = [] # lista con 6 columas
	
	fila_de_datos.append(n) # largo del archivo.txt
	
	# Tiempos de Ejecución en segundos (s):
	fila_de_datos.append(cronometro(lista, distanciaMinima)) #FuerzaBruta
	fila_de_datos.append(cronometro(lista, merge))
	fila_de_datos.append(cronometro(lista, up))
	fila_de_datos.append(cronometro(lista, quick))
	fila_de_datos.append(cronometro(lista, bubble))
	
	return fila_de_datos
########################################################################
#
#
########################################################################
# DATOS
########################################################################
def datos(n, nombre):
	'''
	crea un archivo con formato csv de n-1 filas 
	con las siguientes columnas:
		
	ParesDePuntos, FuerzaBruta, DyC_merge, DyC_up, DyC_quick, DyC_bubble

	La primer columna es el tamaño del conjunto de puntos.
	El resto son las mediciones del tiempo de ejecución de cada algoritmo.
	'''
	with open (nombre, "w") as f:
		for i in range(2,n+1,20): # hardcodeamos el "step" del "range"
			f.write( str(fila_de_datos(i)).strip('[\'\']'))
			f.write("\n")
########################################################################
#
#
########################################################################
def graficar(n):
	'''
	toma un numero (n) -> tamaño del conjunto de datos a procesar
	i.e: 500
	devuelve un archivo datos.pdf con un gráfico de "tamaño de muestra" 
	en función del "tiempo cronometrado" para cada algoritmo evaluado.
	'''
	####################################################################
	# Generar Plot:
	####################################################################
	# 1) carga de datos:
	datos = np.loadtxt("datos.csv", delimiter=",")

	# 2) Título y etiquetas para los ejes:
	plt.title("Ejercicio 1 - datos.pdf")
	plt.xlabel("tam. del conjunto de puntos")
	plt.ylabel("tiempo (s)")

	# 3) plot cada algoritmo estudiado con su label
	plt.plot(datos[:,0], datos[:,1], label="Fuerza bruta")
	plt.plot(datos[:,0], datos[:,2], label="DYC merge")
	plt.plot(datos[:,0], datos[:,3], label="DYC up")
	plt.plot(datos[:,0], datos[:,4], label="DYC quick")
	plt.plot(datos[:,0], datos[:,5], label="DYC bubble")
	plt.legend(loc="upper left")

	# 4) devolver archivo pdf
	plt.savefig("datos.pdf")
	plt.show() # mostrar en pantalla
	
	return None
########################################################################
#
#
########################################################################
#EJECUTAR EJERCICIO1
########################################################################
# Preguntar por consola el valor máximo de n:
flag = False
while not (flag):
	try:
		n =(int(
			input("\n# Bienvenido al Ejercicio1 #\n\n" 
				+ ">Por Favor, ingrese el tamaño final del conjunto " 
				+ "de puntos de entrada: ")))
				
				
		if n >= 3:  #chequear que el tamaño sea al menos un par de puntos
			flag = True	
			datos(n, "datos.csv") # crea entrada.txt y datos.csv 
			graficar(n) # GRAFICAR datos.csv
			
		else:
			print("\n Error: Número inválido. Use un número entero >= 3")
		
	except NameError:
		print("\n Error: Número inválido. Use un número entero >= 3")
########################################################################
