# -*- coding: utf-8 -*-
########################################################################
# Trabajo Práctico
# Ejercicio 1
# Autores: Agustin Dramis y Gabriel Lichtenstein
# Fecha: 03/06/2016
########################################################################
# usage: python3 puntos_de_entrada.py
########################################################################
import random

'''
Para generar puntos de entrada para el ejercicio_1 se implementa
el módulo random.random() que genera números aleatorios entro 0 y 1,
el rango de valores se puede ajustar por medio de operaciones 
aritméticas.

Dada una cantidad n para el conjunto de puntos, devuelve una lista con n
tuplas correspondientes a las coordenadas (x,y)

Las coordenadas estan formateadas en float con un solo decimal.

la fn toma 3 parametros:
n = cantidad de tuplas de coordenadas deseadas
x = valor a multiplicar a la coordenada_x random para aumentar el rango
y = valor a multiplicar a la coordenada_y random para aumentar el rango

para usar el rango de random default usar x,y = 1
'''
########################################################################
coordenada_y = 0.0
coordenada_x = 0.0
########################################################################
def lista_random(n, x, y):
	
	res = []
	for i in range(n):
		
		coordenada_x = float("{0:.1f}".format(random.random()))
		coordenada_y = float("{0:.1f}".format(random.random()))
						
		# ajusto el rango de random usando una multiplicación
		coordenada_x = float("{0:.1f}".format(
							coordenada_x * (x*random.random()) ))
		coordenada_y = float("{0:.1f}".format(
							coordenada_y * (y*random.random()) ))
		
		res.append(tuple((coordenada_x, coordenada_y)))
		# guardar como tupla en la lista res
	
	return res
########################################################################
#
#
########################################################################
# Entrada de lista de puntos
########################################################################
def entradaRandom(n, archivo):
	'''
	Genera un archivo de puntos random en el formato del ejercicio 1
	
	p.ej:	
	1.3 4.4
	1.0 5.3
	0.0 2.3
	0.5 5.0
	
	'''
	lista = lista_random(n, 50, 50) # 50 esta hardcodeado en este caso
	with open (archivo, "w") as f:
		for i in range(n):
			
			punto = str(lista[i][0]) + " " + str(lista[i][1])
			f.write(punto)
			f.write("\n")
########################################################################
if __name__ == "__main__":
	print ("Me estan ejecutando")
