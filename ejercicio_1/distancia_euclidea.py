# -*- coding: utf-8 -*-
########################################################################
# Trabajo Práctico
# Ejercicio 1
# Autores: Agustin Dramis y Gabriel Lichtenstein
# Fecha: 03/06/2016
########################################################################
from math import sqrt

from preprocesamiento import merge, up, bubble, quick

########################################################################
# Mínima Distancia Euclidea por "Fuerza Bruta"
########################################################################
'''
Función que calcula la distancia euclídea entre dos puntos, recibidos 
como parámetros en forma de tuplas (x,y)
'''
def distEuclidea(punto1,punto2):
	return  sqrt(((punto1[0] - punto2[0]) * ((punto1[0] - punto2[0]))) 
				 + ((punto1[1]- punto2[1]) * (punto1[1]- punto2[1])))
	

'''
Funcion que calcula la distancia euclidea entre un punto de una lista 
y el punto de la posición 0:
(x0,y0),(xi,yi)

toma dos parametros:
a = listaDePuntos(fn)
i = indice a iterar -> a[i]

(x0,y0) son siempre el primer punto de la lista a:
x0 = a[0][0]
y0 = a[0][1]
'''
def distEuclideaAlPrimero(a,i):
	return  distEuclidea(a[0],a[i])
	
	
'''
2) Función que busca y devuelve el par de puntos de la lista "a" cuya 
   distancia euclídea es mínima.
   
   Se utiliza un algoritmo de "Fuerza Bruta" con O(n²), es decir se
   calculan las distancias entre todos los pares de puntos de la lista a
   
   Se toma el primer punto de la lista, se lo compara con el resto y se
   obtiene la distanciaMinima. Se crea una lista nueva sin el primer 
   punto, se lo compara con el resto y se obtiene la distanciaMinima. 
   Se repite n veces hasta que solo quede un par de puntos a comparar 
   en la lista. La distanciaMínima final resulta de comparar entre si
   la distanciaMinima obtenida en cada recursion.
   
   Se implementa una función recursiva tomando una lista a 
   con un punto (x0,y0) menos en cada recursión: 
   distancia = distanciaMinima(a[1:])   
'''	
def distanciaMinima(a):
	
	if len(a) == 2: #cuando solo queda un par a comparar:
		return (a[0],a[1])
	
	res = (a[0],a[1])
	distRes = distEuclidea(res[0],res[1])
	for i in range(2,len(a)):
		distancia = distEuclidea(a[0],a[i])
		
		if distancia < distRes:
			res = (a[0],a[i])
			distRes = distEuclidea(a[0],a[i])
			# almacenar el 2° punto del par con distanciaMinima:
			x1 = a[i][0]
			y1 = a[i][1]
	
	a = a[1:] #para que pueda llamar a[0][0] actual con x0
	resListaAcotada = distanciaMinima(a) #llamada recursiva
	distancia = distEuclidea(resListaAcotada[0],resListaAcotada[1])
	
	if distancia < distRes:
		res = resListaAcotada
	
	return res # devuelve el par de puntos con distancia mínima
########################################################################
#
#
########################################################################
# Mínima Distancia Euclidea por "Divide, Conquer & Combine"
########################################################################	
'''
Función que determina la distancia en el eje x entre 2 puntos
'''
def distanciaEnXALaMitad(punto,mitad):
	return abs(punto[0] - mitad)

				
'''
Implementación de distanciaMinimaDyC.
La función utiliza dos casos base, que son las listas de 2 o 3 
elementos.
En caso de encontrarse por fuera de los casos base, la lista es 
preprocesada con el algoritmo recibido como parámetro, y la distancia
mínima es calulada recursivamente para las mitades de la lista 
(etapa Divide).
De las distancias mínimas obtenidas para ambas mitades, la función toma 
la menor, y la utiliza como cota para llamar a la función anterior 
(etapa combine).
'''		
def distanciaMinimaDyC(a, algoritmo):
	if len(a) == 2:
		return (a[0],a[1])
	if len(a) == 3:
		return distanciaMinima(a)
	a = algoritmo(a) #Preprocesamiento
	medio = len(a)//2
	mitad = a[medio][0]
	'''
	Estas dos variables son los pares de puntos de las 
	distintas mitades con distancia mínima.
	Las dos variables siguientes son sus respectivas distancias.
	'''
	minimaPrimeraMitad = (distanciaMinimaDyC(a[:medio],algoritmo))
	minimaSegundaMitad = distanciaMinimaDyC(a[medio:],algoritmo)
	distResPrimerMitad = (distEuclidea(minimaPrimeraMitad[0],minimaPrimeraMitad[1]))
	distResSegundaMitad = distEuclidea(minimaSegundaMitad[0],minimaSegundaMitad[1])
	if distResPrimerMitad < distResSegundaMitad:
		cota = distResPrimerMitad
		res = minimaPrimeraMitad
	else:
		cota = distResSegundaMitad
		res = minimaSegundaMitad
	aAcotado= []
	for i in range(len(a)):
		if distanciaEnXALaMitad(a[i],mitad) <= cota:
			aAcotado.append(a[i])
	if len(aAcotado) >= 2:
		resAcotado = distanciaMinima(aAcotado)
		distResAcotado = distEuclidea(resAcotado[0],resAcotado[1])
		if distResAcotado < cota:
			res = resAcotado
	return res
########################################################################
if __name__ == "__main__":
	print ("Me estan ejecutando")
