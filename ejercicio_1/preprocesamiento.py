# -*- coding: utf-8 -*-
########################################################################
# Trabajo Práctico
# Ejercicio 1 - fn aux preprocesamiento
# Autores: Agustin Dramis y Gabriel Lichtenstein
# Fecha: 03/06/2016
########################################################################
#
#
########################################################################
# Preprocesamiento - "list sorters"
########################################################################
'''
a) Preprocesamiento: la lista de puntos debe encontrarse ordenada en 
su coordenada x. 

Implemente:
merge sort,
bubble sort,
upsort y
quick sort, 
y decida que algoritmo utilizar en funcion del valor de la cadena
de caracteres indicada por el parametro algoritmo.

Los valores posibles son los siguientes:
merge: utiliza merge sort.
bubble: utiliza bubble sort.
up: utiliza upsort.
quick: utiliza quick sort.

En caso de que el algoritmo especificado no corresponda a ninguno
de los anteriores, debe fallar con una excepcion de tipo NameError.

b) Dividir los puntos en dos mitades cortando sobre el eje x.
c) Buscar recursivamente las distancias menores en ambas mitades, y
   quedarse con la menor de ambas.
d) Combinar  dicho  resultado  con  las  distancias  entre  pares 
   de puntos que se encuentren cada uno en una mitad diferente.

Ayuda 2: puede utilizar la menor de las distancias encontradas en
los pasos recursivos como cota para determinar cuanto debe alejarse
hacia cada lado en la coordenada x de la recta que corta ambas 
mitades, ya que la lista de puntos se encuentra ordenada en x.
'''
########################################################################
#
#
########################################################################
# 1) Merge Sort
########################################################################
'''
Implementación de merge sort.	
La función auxiliar uneListas realiza la etapa "merge", asumiendo que 
recibe dos listas que ya están ordenadas. Al comienzo, asigna una lista 
vacía al resultado. Luego, la función agrega al resultado cada elemento 
de "a", antecedido por todos los elementos de "b" que sean menores. Al 
terminar de recorrer la lista "a", sólo quedarán los elementos de "b" 
que sean iguales o mayores al mayor de "a", por lo que la función los 
agrega al resultado y lo devuelve.
'''

def uneListas(a,b):
	res = []
	i = 0
	j = 0
	while i < len(a):
		while j < len(b) and b[j][0] < a[i][0]:
			res.append(b[j])
			j = j + 1
		res.append(a[i])
		i = i + 1
	while j < len(b):
		res.append(b[j])
		j = j + 1
	return res
	
	
'''
Cuerpo principal de merge sort. La función utiliza dos casos base, que 
son las listas de uno y dos elementos, cuyo ordenamento es trivial. Para
 listas más grandes, la función llama a su función auxiliar sobre las 
 mitades de la lista, las cuales ordena mediante una llamada recursiva 
 sobre sí misma
'''
	
def merge(a):
	if len(a) < 2:
		return a
	if len(a) == 2:
		if a[0][0] > a[1][0]:
			a[0],a[1] = a[1],a[0]
		return a
	medio = len(a) // 2
	return uneListas(merge(a[:medio]),merge(a[medio:]))
########################################################################
#
#
########################################################################
# 2) Bubble Sort
########################################################################	
'''
Implementación de bubble sort. En el loop "while" interno, el algoritmo 
recorre la lista comparando cada elemento (menos el último) con el 
siguiente, y realizando un swap entre ellos en caso de que estén 
desordenados. Mediante el uso del flag "ordenada", que indica el estado 
de la lista que ingresa al loop interno, deja de recorrer la lista
cuando en una recorrida no se realiza ningún cambio, es decir, cuando 
ya está ordenada. Una vez que esto pasa, devuelve la lista.
'''	
def bubble(a):
	ordenada = False
	while not ordenada:
		i = 0
		ordenada = True
		while i + 1 < len(a):
			if a[i+1][0] < a[i][0]:
				a[i],a[i+1] = a[i+1],a[i]
				ordenada = False
			i = i + 1
	return a
	
########################################################################
#
#
########################################################################
# 3) UpSort
########################################################################
'''
Implementación de upsort. 

1) Dada una lista, se busca el entero más grande y el indice donde 
	se encuentra (usando la fn auxiliar maxPos). 

	1.1) La busqueda se hace en el rango(0, actual).

		actual = la última posición de la lista.

2) Se permuta el entero más grande que se encontro a la ultima posición 
	de la lista.

3) Se busca el entero más grande en el rango(0, actual-1) hasta que 
	actual sea igual a 0.
'''
def up(a):
	actual = (len(a)-1)
	m = 0
	while (actual > 0):
		m = maxPos(a, 0, actual)
		a[m], a[actual] = a[actual], a[m]
		actual -=1
	return a

def maxPos(a, i, actual):
	# cual es la posicion del valor máximo en una lista.
	res = i
	for pos in range(i, actual+1):
		if a[res][0] < a[pos][0]: #chequea los x de la tupla (x,y)
			res = pos
	return res
########################################################################
#
#
########################################################################
# 4) QuickSort
########################################################################
'''
Implementación de quicksort.
Al igual que mergesort, la función utiliza dos casos base, de trivial 
ordenamiento: Una lista de dos elementos, que sólo requiere un paso 
para ordenarse, y la lista de menos de dos elementos (uno o vacía), que
siempre se encuentran "ordenandas".
De no encontrarse en ninguno de los casos base, la función toma al 
primer elemento de la lista como "pivot", y recorre el resto generando 
tres particiones: una primer mitad con los elementos menores al pivot,
el pivot en sí, y una segunda mitad con los elementos mayores al pivot.
Finalmente, las dos mitades son ordenadas mediante una llamada recursiva,
y las tres particiones son concatenadas.
'''
def quick(a):
	if len (a) < 2:
		return a
	if len(a) == 2:
		if a[0][0] > a[1][0]:
			a[0],a[1] = a[1],a[0]
		return a
	pivot = [a[0]]
	primeraMitad = []
	segundaMitad = []
	for i in range (1,len(a)):
		if a[i][0] < a[0][0]:
			primeraMitad.append(a[i])
		else:
			segundaMitad.append(a[i])
	return quick(primeraMitad) + pivot + quick(segundaMitad)
########################################################################
if __name__ == "__main__":
	print ("Me estan ejecutando")
