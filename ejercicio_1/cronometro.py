# -*- coding: utf-8 -*-
########################################################################
# Trabajo Práctico
# Ejercicio 1 - fn aux cronometro.py
# Autores: Agustin Dramis y Gabriel Lichtenstein
# Fecha: 03/06/2016
########################################################################
from puntos_random import lista_random

from distancia_euclidea import distanciaMinima, distanciaMinimaDyC
from preprocesamiento import merge, up, bubble, quick

import time

########################################################################
# Cronometrar tiempo de ejecución
########################################################################
def cronometro(lista, algoritmo):
	'''
	Toma el tiempo en segundos de cuanto tarda cada algoritmo con listas
	de distinto tamaño.
	
	Los algoritmos pueden ser:
	Por Fuerza Bruta: distanciaMinima
	Por Divide&Conquer: up, quick, merge o bubble
	'''
	if algoritmo == distanciaMinima:
		t0 = time.time()
		distanciaMinima(lista)
		t1 = time.time()
		return (t1 -t0)
	else:
		t2 = time.time()
		distanciaMinimaDyC(lista, algoritmo)
		t3 = time.time()
		return (t3 -t2)
########################################################################
if __name__ == "__main__":
	print ("Me estan ejecutando")
