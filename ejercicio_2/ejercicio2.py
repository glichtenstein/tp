# -*- coding: utf-8 -*-
#!/usr/bin/env python
########################################################################
# Trabajo Práctico - Grupo 4
# Ejercicio 2 - Sublistas
# Autores: Agustin Dramis y Gabriel Lichtenstein
# Fecha: 03/06/2016
########################################################################
# usage: python3 ejercicio2.py
########################################################################
#
#
########################################################################
def sufijoSubAscN(a,i,n):
	'''
	Implementación de sufijoSubAscN. La función devuelve True 
	si la sublista de "a" que comienza en la posición "i" y 
	tiene longitud "n" es ascendente.
	'''
	res = True
	pos = i
	while pos < i + n - 1 and res:
		if not a[pos] < a[pos + 1]:
			res = False
		pos = pos + 1
	return res
########################################################################
#
#
########################################################################
def subListaAsc(a,n):
	'''
	Implementación de subListaAsc. La función devuelve True si dentro 
	de la lista "a" hay una sublista ascendente de longitud "n".
	'''
	i = 0
	while i + n <= len(a) and not sufijoSubAscN(a,i,n):
		i = i + 1
	return i + n <= len(a) 
########################################################################
