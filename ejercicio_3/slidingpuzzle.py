# -*- coding: utf-8 -*-
#!/usr/bin/env python
########################################################################
# Trabajo Práctico - Grupo 4
# Ejercicio 3 - Slidding Buho Puzzle
# Autores: Agustin Dramis y Gabriel Lichtenstein
# Fecha: 03/06/2016
########################################################################
# usage: ./tppuzzle.py
########################################################################
#
#
########################################################################
from __future__ import print_function
from copy import copy

UP, LEFT, DOWN, RIGHT = 0, 1, 2, 3
########################################################################
#
#
########################################################################
def contrario(i):
	'''
	Función que devuelve el movimiento contrario al elegido.
	p.ej:
	i = 0 ("UP")
	devuelve i = 2 ("DOWN")	
	'''
	if i < 2:
		return i + 2
	if i >= 2:
		return i - 2
########################################################################
#
#
########################################################################
class Rompecabezas(object):
########################################################################	
	# Class init
	####################################################################
	def __init__(self, width, height, parent=None):
		self.parent = parent

		######################################
		# agregar atributos acá a continuación
		#
		######################################
        
		# atributos que debe tener la clase Rompecabezas 
		# cuando se inicia :	
			
		# tablero "puzzle" de números ordenados (a.k.a resuelto):
		self.puzzle = [] 		
		for i in range(height):
			fila = []
			for j in range(width):
				fila.append(str((i*width)+j+1))
			self.puzzle.append(fila)
		self.puzzle[height-1][width-1] = ' ' #espacio vacio del "puzzle"
	####################################################################
	#
	#		
	####################################################################
	def update(self):
		if self.parent is not None:
			self.parent.redraw()
	####################################################################
	#
    ######################################
    # COMPLETAR LOS MÉTODOS A CONTINUACIÓN
    #
    ######################################
    #
    ####################################################################
	def cargar(self, fn):
		# carga un archivo.txt de "puzzle" como lista "[self.puzzle]"
		self.puzzle = []
		with open(fn, 'r') as f:
		
			for line in f:
				line = line.rstrip('\n')
				line = line.split('\t')
				self.puzzle.append(line)
		
		return self.puzzle	
	####################################################################
	#
	#
	####################################################################
	def get(self, i, j):
		# devuelve una string con el valor almacenado en la 
		# fila i, columna j.
		return (str(self.puzzle[i][j]))
	####################################################################
	#
	#
	####################################################################
	def __str__(self):
		'''
		devuelve una cadena de caracteres con una representacion
		textual del rompecabezas. La misma debe corresponder al formato 
		indicado en el apendice B.
		'''
		aImprimir = copy(self.puzzle) # Evita modificar el puzzle original
		for line in range(len(aImprimir)):
			aImprimir[line] = '\t'.join(aImprimir[line])
		
		return '\n'.join(aImprimir)
	####################################################################
	#
	#
	####################################################################
	def ancho(self):
		# devuelve la cantidad de columnas en el rompecabezas:		
		return (len(self.puzzle[0]))
	####################################################################
	#
	#
	####################################################################
	def alto(self): #agregue parametro height para inicializar
		# devuelve la cantidad de filas en el rompecabezas:
		return (len(self.puzzle))
	####################################################################
	#
	#
	####################################################################
	def resuelto(self):
		'''
		devuelve True si el rompecabezas está resuelto, es decir, si la 
		secuencia almacenada se encuentra ordenada crecientemente, de
		izquierda a derecha y de arriba a abajo, o False en otro caso.
		'''
		res = True
		i = 0
		while i < self.alto() and res:
			j = 0
			while j < self.ancho() and res:
				try:
					if int(self.get(i,j)) == (self.ancho()*i + j +1):
						res = True
					else:
						res = False
				except ValueError:
					if ((i == self.alto() - 1) 
								and (j == self.ancho() - 1) 
								and (self.get(i,j) == ' ')):
									res = True
					else:
						res = False
				j = j + 1
			i = i + 1
		return res
	####################################################################
	#
	#
	####################################################################
	def mover(self, direccion):
		'''
		intercambia el espacio vacío con la pieza que se encuentra en 
		la dirección especificada por dirección, en caso de que este 
		movimiento sea posible. En caso de no serlo, deja el tablero 
		intacto. 
		
		Devuelve True en caso de éxito y False si no es posible el 
		movimiento. 
		
		Las direcciones pueden ser 0, 1, 2, 3.
		'''
		vacio = False
		i = 0
		while i < self.alto() and not vacio:
			j = 0
			while j < self.ancho() and not vacio:
				vacio = (self.get(i,j)==' ')
				if vacio:
					f,c = i,j # Guarda coordenadas de pieza vacía 
				j = j + 1
			i = i + 1
		
		# UP #
		if direccion == 0:
			if f - 1 < 0:
					return False
			try:
				#swap
				self.puzzle[f][c],self.puzzle[f-1][c] \
				= self.puzzle[f-1][c],self.puzzle[f][c]
				return True
			except IndexError:
				return False
		
		# LEFT #
		if direccion == 1:
			if c - 1 < 0:
					return False
			else:
				try:
					#swap
					self.puzzle[f][c],self.puzzle[f][c-1] \
					= self.puzzle[f][c-1],self.puzzle[f][c]
					return True
				except IndexError:
					return False
		
		# DOWN #
		if direccion == 2:
			try:
				#swap
				self.puzzle[f][c],self.puzzle[f+1][c] \
				= self.puzzle[f+1][c],self.puzzle[f][c]
				return True
			except IndexError:
				return False
		
		# RIGHT #
		if direccion == 3:
			try:
				#swap
				self.puzzle[f][c],self.puzzle[f][c+1] \
				= self.puzzle[f][c+1],self.puzzle[f][c]
				return True
			except IndexError:
				return False
	####################################################################
	#
	#
	####################################################################
	def guardar(self, fn):
		'''
		Guarda la representación textual del rompecabezas en el archivo 
		cuyo nombre se encuentra indicado por "fn"
		'''
		with open(fn,'w') as f:
			f.write(self.__str__())
	####################################################################
	#
	#
	####################################################################
	def resolver(self, n,origen=None):
		'''
		Implementación de resolver. La función posee como caso 
		base n==0, es decir, cuando se la llama con 0 pasos disponibles, 
		y en ese caso, devuelve True si el tablero está resuelto. 
		
		Por fuera del caso base, la función llama a "mover" en las 
		4 direcciones posibles,	y en caso de no haber resuelto 
		el puzzle, se llama recursivamente con un paso menos disponible. 
		Si la llamada recursiva no logró resolver el tablero, se vuelve 
		a la posición original, antes de probar otra dirección. 
		El parámetro opcional "origen", que es dado	en todas 
		las llamadas recursivas, descarta los caminos que comiencen 
		retrocediendo al lugar de "origen" del nivel recursivo anterior
		'''
		# resolver por backtracking:
		if n == 0:
			return self.resuelto()
		res = self.resuelto()
		i = 0
		while i <= 3 and not res:
			if i != origen: 
				if self.mover(i):
					self.update()
					res = self.resuelto()
					if not res:
						res = self.resolver(n-1,contrario(i))
					if not res:
						self.mover(contrario(i))
						self.update()
			i = i + 1		
		return res
########################################################################
if __name__ == '__main__':
	# acá pueden completar con algunas pruebas para usar con el 
	# intérprete interactivo
	pass
