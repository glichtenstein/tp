# -*- coding: utf-8 -*-
#!/usr/bin/env python
########################################################################
# Trabajo Práctico - Grupo 4
# Ejercicio 4 - Arbol Testing
# Autores: Agustin Dramis y Gabriel Lichtenstein
# Fecha: 03/06/2016
########################################################################
# usage: python test.py
########################################################################
#
#
########################################################################
import unittest
from arbol import Arbol
########################################################################
#
#
########################################################################
class TestConstructor(unittest.TestCase):
########################################################################
    def test_cuando_arbol_mal_formado(self):
        with self.assertRaises(TypeError): # chequea el try, except
			Arbol(None,51,2) #arbol mal formado
########################################################################
#
#
########################################################################
class TestVacio(unittest.TestCase):
########################################################################	
    def test_cuando_arbol_vacio_es_true(self):
        arbol = Arbol()
        self.assertTrue(arbol.vacio())
	
    def test_cuando_arbol_no_vacio_es_false(self):
        arbol = Arbol(2)
        self.assertFalse(arbol.vacio())
########################################################################
#
#
########################################################################
class TestRaiz(unittest.TestCase):
########################################################################
    def test_devuelve_la_raiz(self):
        arbol = Arbol(2)
        self.assertEqual(arbol.raiz(), 2)
########################################################################
#
#
########################################################################
class TestIzquierda(unittest.TestCase):
########################################################################
	def test_si_hay_izquierda_la_devuelve(self):
		arbol = Arbol(1,Arbol(2))
		self.assertEqual(arbol.izquierda(),Arbol(2))

	def test_si_no_hay_izquierda_devuelve_arbol_vacio(self):
		arbol = Arbol(1)
		self.assertEqual(arbol.izquierda(),Arbol())
########################################################################
#
#
########################################################################
class TestDerecha(unittest.TestCase):
########################################################################
	def test_si_hay_derecha_la_devuelve(self):
		arbol = Arbol(1,None,Arbol(2))
		self.assertEqual(arbol.derecha(),Arbol(2))

	def test_si_no_hay_derecha_devuelve_arbol_vacio(self):
		arbol = Arbol(1)
		self.assertEqual(arbol.derecha(),Arbol())
########################################################################
#
#
########################################################################
class Test__eq__(unittest.TestCase):
########################################################################	
	def test_si_son_iguales_es_True(self):
		arbol1 = Arbol(1,Arbol(3),Arbol(5,(Arbol(2))))
		arbol2 = Arbol(1,Arbol(3),Arbol(5,(Arbol(2))))
		self.assertTrue(arbol1==arbol2)
	
	def test_si_son_distintos_es_False(self):
		arbol1 = Arbol(1,Arbol(3),Arbol(5,(Arbol(3))))
		arbol2 = Arbol(1,Arbol(3),Arbol(5,(Arbol(2))))
		self.assertFalse(arbol1==arbol2)
########################################################################
#
#
########################################################################
class TestFind(unittest.TestCase):
########################################################################	
	def test_si_esta_devuelve_True(self):
		arbol = Arbol(1,Arbol(3),Arbol(5,(Arbol(7))))
		self.assertTrue(arbol.find(7))
	
	def test_si_no_esta_devuelve_False(self):
		arbol = Arbol(1,Arbol(3),Arbol(5,(Arbol(7))))
		self.assertFalse(arbol.find(15))
########################################################################
#
#
########################################################################
class TestEspejo(unittest.TestCase):
########################################################################	
	def test_espejo_es_reversible(self):
		arbol = arbol = Arbol(1,\
		Arbol(2, Arbol(3), Arbol(4)),\
		Arbol(5, Arbol(6), Arbol(7)))
		self.assertEqual(arbol,arbol.espejo().espejo())

	def test_espejo_funciona_con_una_rama(self):
		arbol = Arbol(1,None,Arbol(5,Arbol(6),Arbol(7))) #rama vacia
		arbol_espejado = Arbol(1,Arbol(5,Arbol(7),Arbol(6)))
########################################################################
#
#
########################################################################
class TestOrders(unittest.TestCase):
########################################################################
	def test_preorder_da_orden_correcto(self):
		arbol = Arbol(1,\
		Arbol(2, Arbol(3), Arbol(4)),\
		Arbol(5, Arbol(6), Arbol(7)))

		self.assertEqual(arbol.preorder(),[1,2,3,4,5,6,7])

	def test_posorder_da_orden_correcto(self):
		arbol = Arbol(1,\
		Arbol(2,Arbol(3),Arbol(4)),\
		Arbol(5,Arbol(6),Arbol(7)))
		
		self.assertEqual(arbol.posorder(),[7,6,5,4,3,2,1])

	def test_inorder_da_orden_correcto(self):
		arbol = Arbol(1,\
		Arbol(2, Arbol(3), Arbol(4)),\
		Arbol(5, Arbol(6), Arbol(7)))
		self.assertEqual(arbol.inorder(),[3,2,4,1,6,5,7])

	def test_preorder_saltea_Nones(self):
		arbol = Arbol(1,None,Arbol(5,Arbol(6),Arbol(7)))
		self.assertEqual(arbol.preorder(),[1,5,6,7])

	def test_posorder_saltea_Nones(self):
		arbol = Arbol(1,None,Arbol(5,Arbol(6),Arbol(7)))
		self.assertEqual(arbol.posorder(),[7,6,5,1])

	def test_inorder_saltea_Nones(self):
		arbol = Arbol(1,None,Arbol(5,Arbol(6),Arbol(7)))
		self.assertEqual(arbol.inorder(),[1,6,5,7])
########################################################################
if __name__ == '__main__':
    unittest.main()
