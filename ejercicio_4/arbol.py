# -*- coding: utf-8 -*-
#!/usr/bin/env python
########################################################################
# Trabajo Práctico - Grupo 4
# Ejercicio 4 - Arbol Testing
# Autores: Agustin Dramis y Gabriel Lichtenstein
# Fecha: 03/06/2016
########################################################################
# usage: python test.py
########################################################################
#
#
########################################################################
class Arbol(object):
########################################################################	
	'''
	Clase Arbol() que implementa un "árbol binario", que tiene raíz,
	una rama derecha y una izquierda.
	'''	
	####################################################################
	def __init__(self, raiz=None, izquierda=None, derecha=None):
		'''
		Inicializa un árbol como raíz del árbol e izquierda y derecha 
		como subárboles. Si raíz es "None", se considera que el árbol
		es vacio.
		'''
		self.r = raiz
		if raiz is not None:
			if izquierda:
				self.i = izquierda
			else:
				self.i = Arbol() #Arbol vacio
			if derecha:
				self.d = derecha
			else:
				self.d = Arbol()
		else:
			if izquierda is not None or derecha is not None:
				raise TypeError("arbol mal formado")
	####################################################################
	#
	#
	####################################################################
	def vacio(self):
		'''
		indica si el árbol no posee ningún elemento
		'''
		return self.r == None
	####################################################################
	#
	#
	####################################################################
	def raiz(self):
		'''
		devuelve el elemento correspondiente a la raíz del árbol
		'''
		return self.r
	####################################################################
	#
	#
	####################################################################
	def izquierda(self):
		'''
		devuelve el subárbol correspondiente a la rama izquierda
		'''
		if not self.vacio():# check vacio
			return self.i
	####################################################################
	#
	#
	####################################################################
	def derecha(self):
		'''
		devuelve el subárbol correspondiente a la rama derecha
		'''
		if not self.vacio():# check vacio
			return self.d
	####################################################################	
	#
	#
	####################################################################
	def __eq__(self,otro_arbol=None):
		'''
		indica si otro árbol es igual a self. Decimos que dos árboles 
		son iguales si tienen igual raíz, y sus subárboles a izquierda y
		derecha son iguales.
		'''
		if self.vacio() and otro_arbol.vacio(): # check vacio
			return True
	
		return (self.raiz() == otro_arbol.raiz() #acá == son el default
			and self.izquierda() == otro_arbol.izquierda()
			and self.derecha() == otro_arbol.derecha())
	####################################################################
	#
	#
	####################################################################	
	def find(self,a):
		'''
		indica si el elemento "a" esta en el árbol
		'''
		if self.vacio(): # si es árbol vació no ésta "a":
			return False
		elif self.raiz() == a: # si "a" es la raíz:
				return True
		else: # si "a" esta en rama derecha o izquierda:
			return (self.izquierda().find(a) or self.derecha().find(a)) 
	####################################################################
	#
	#
	####################################################################
	def espejo(self):
		'''
		devuelve un nuevo árbol "espejado" que resulta de intercambiar
		los subárboles espejados de izquierda a derecha y viceversa.
		'''
		if not self.vacio(): # check vacio
			return Arbol(self.raiz(), \
			self.derecha().espejo(), self.izquierda().espejo())
		return None
	####################################################################
	#
	#
	####################################################################	
	def preorder(self):
		'''
		retorna una lista de enteros que resulta de colocar la raíz, 
		seguido del preorder de las ramas izquierda y derecha.
		'''
		res = []
		if not self.vacio():
			res.append(self.raiz())
			for i in self.i.preorder():
				res.append(i)
			for j in self.d.preorder():
				res.append(j)
		return res
	####################################################################	
	def posorder(self):
		'''
		retorna una lista de enteros que resulta de colocar la raíz, 
		seguido del posorder de las ramas izquierda y derecha.
		'''
		res = []
		if not self.vacio():
			for j in self.d.posorder():
				res.append(j)
			for i in self.i.posorder():
				res.append(i)
			res.append(self.raiz())	
		return res
	####################################################################
	#
	#
	####################################################################
	def inorder(self):
		'''
		retorna una lista de enteros que resulta ser el recorrido de 
		los elementos del parámetro implícito de izquierda a derecha 
		como si se los aplastara 
		(la raíz queda entre las ramas izquierda y derecha).
		'''
		res = []
		if not self.vacio():
			for i in self.i.inorder():
				res.append(i)
			res.append(self.raiz())
			for j in self.d.inorder():
				res.append(j)
		return res
########################################################################
